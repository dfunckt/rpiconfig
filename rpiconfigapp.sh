#!/bin/sh

#-----------------------------------------------------------------------------
# This script requires the following two environment variables to be set:
#   $ export API_HOST='https://api.resin.io'
#   $ export API_TOKEN='<user-api-token-from-dashboard-prefs>'
#-----------------------------------------------------------------------------

usage() {
  echo 'Quick and dirty script to manage config.txt for RPi devices via resin.io.'
  echo
  echo 'Use this script to manage config variables applying to all devices of an app.'
  echo
  echo 'Make sure you have set API_TOKEN before running the script, for example '
  echo 'via: $ export API_TOKEN="srdg4vcRSthDTCtc..."'
  echo
  echo 'Available commands:'
  echo
  echo '  create [APP_ID] [CONFIG_KEY] [CONFIG_VALUE]'
  echo '  get [APP_ID] [CONFIG_KEY]'
  echo '  update [CONFIG_ID] [CONFIG_VALUE]'
  echo '  delete [CONFIG_ID]'
  echo
}

if [ "$#" -lt 2 ]; then
  usage
  exit
fi

DEFAULT_API_HOST='https://api.resin.io'
API_HOST=${API_HOST:-$DEFAULT_API_HOST}

if [ -z $API_TOKEN ]; then
  echo 'You must set the API_TOKEN environment variable before using this script.'
  exit
fi

KEY_PREFIX=''

#-----------------------------------------------------------------------------
# Creating a new config.txt variable applying to all devices
#-----------------------------------------------------------------------------

create() {
  local APP_ID=$1
  local CONFIG_KEY="$2"
  local CONFIG_VALUE="$3"
  curl -X POST "$API_HOST/ewa/environment_variable" \
    -H 'Content-Type: application/json;charset=UTF-8' \
    -H "Authorization: Bearer $API_TOKEN" \
    --data "{\"name\":\"${KEY_PREFIX}${CONFIG_KEY}\",\"value\":\"$CONFIG_VALUE\",\"application\":$APP_ID}"
}

# The response contains the created variable's primary key (id).
# Can be used to update its value, or delete it later on.

#-----------------------------------------------------------------------------
# Getting an application-wide config variable
#-----------------------------------------------------------------------------

get() {
  local APP_ID=$1
  local CONFIG_KEY="$2"
  curl -X GET "$API_HOST/ewa/environment_variable?\$filter=((application%20eq%20${APP_ID})%20and%20(name%20eq%20%27${KEY_PREFIX}${CONFIG_KEY}%27))" \
    -H "Authorization: Bearer $API_TOKEN"
}

# The response contains the variable's primary key (id) and value.


#-----------------------------------------------------------------------------
# Updating the value of an application-wide config variable
#-----------------------------------------------------------------------------

update() {
  local CONFIG_ID=$1
  local CONFIG_VALUE="$2"
  curl -X PATCH "$API_HOST/ewa/environment_variable($CONFIG_ID)" \
    -H 'Content-Type: application/json;charset=UTF-8' \
    -H "Authorization: Bearer $API_TOKEN" \
    --data "{\"value\":\"$CONFIG_VALUE\"}"
}

#-----------------------------------------------------------------------------
# Deleting an application-wide config variable
#-----------------------------------------------------------------------------

delete() {
  local CONFIG_ID=$1
  curl -X DELETE "$API_HOST/ewa/environment_variable($CONFIG_ID)" \
    -H "Authorization: Bearer $API_TOKEN"
}

#-----------------------------------------------------------------------------

main() {
  local command=$1
  shift
  case $command in
    create)
      create "$@"
      ;;
    get)
      get "$@"
      ;;
    update)
      update "$@"
      ;;
    delete)
      delete "$@"
      ;;
    *)
      usage
      ;;
  esac
  echo
}

main $@
exit $?
