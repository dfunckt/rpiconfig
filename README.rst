Overview
========

Scripts to manage config.txt for RPi devices via resin.io.

Make sure you have set API_TOKEN before running the script, for example
via: ``$ export API_TOKEN='srdg4vcRSthDTCtc...'``

Getting started
+++++++++++++++

1. Clone the scripts and enter the directory with::

    $ git clone https://bitbucket.org/dfunckt/rpiconfig.git
    $ cd rpiconfig/

2. Export your API token as an environment variable::
    
    $ export API_TOKEN='<your api token>'
   
   You may get the API token from `Dashboard / Preferences / Auth Token`_.

.. _Dashboard / Preferences / Auth Token: https://dashboard.resin.io/preferences 

Creating an application-wide config variable
--------------------------------------------

Let's say we need to set the "sdtv_mode" config variable globally (i.e.
applying to all devices of an application). ``config.txt`` variables must be
prefixed by ``RESIN_HOST_CONFIG_`` to be picked up by the resin.io agent on
the device so "sdtv_mode" must be written as ``RESIN_HOST_CONFIG_sdtv_mode``.

We will also need the application's ID (``APP_ID``), which we can get
from the Dashboard by going to application page and checking the URL, which
will be in the format ``/apps/15/devices``. The number ``15`` there is the
number we want.

In terminal, type::

    $ ./rpiconfigapp.sh create 15 RESIN_HOST_CONFIG_sdtv_mode "value"

This will create the variable and print a bunch of JSON data in response.
If you examine this output, you'll see the created variable's ID (``CONFIG_ID``),
which can be used later to update or delete this variable.

Getting an application-wide config variable
--------------------------------------------

To fetch the current value and ID of, let's say "sdtv_mode", you may do the
following::

    $ ./rpiconfigapp.sh get 15 RESIN_HOST_CONFIG_sdtv_mode

Overriding a config variable for a specific device
--------------------------------------------------

Let's say we want to override the "sdtv_mode" config variable for a specific
device. The same steps would apply if we'd want to create completely new
variables that are device-specific.

For this, we'll similarly need the device's ID (``DEVICE_ID``), which we can
get from the Dashboard's device summary page URL: ``/apps/15/devices/1234/summary``.
The number ``1234`` is the number we want.

In terminal, type::

    $ ./rpiconfigdev.sh create 1234 RESIN_HOST_CONFIG_sdtv_mode "newvalue"

To fetch the current value and ID of a config variable that is specific to a
device, you may do the following::

    $ ./rpiconfigdev.sh get 1234 RESIN_HOST_CONFIG_sdtv_mode

Available commands
++++++++++++++++++

rpiconfigapp.sh
---------------

Use this script to manage config variables applying to all devices of an app.

- ``create [APP_ID] [CONFIG_KEY] [CONFIG_VALUE]``
- ``get [APP_ID] [CONFIG_KEY]``
- ``update [CONFIG_ID] [CONFIG_VALUE]``
- ``delete [CONFIG_ID]``

rpiconfigdev.sh
---------------

Use this script to manage device-specific config variables and overrides.

- ``create [DEVICE_ID] [CONFIG_KEY] [CONFIG_VALUE]``
- ``get [DEVICE_ID] [CONFIG_KEY]``
- ``update [CONFIG_ID] [CONFIG_VALUE]``
- ``delete [CONFIG_ID]``
